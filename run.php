<?php
	if ( ! file_exists( 'madeline.php' ) ) {
		copy( 'https://phar.madelineproto.xyz/madeline.php' , 'madeline.php' ) ;
	}
	include( 'madeline.php' ) ;

	list( , $phone , $msg ) = $argv ;

	$mph = new \danog\MadelineProto\API( 'session.madeline' ) ;
	$mph->start( ) ;

	$contacts = $mph->contacts->importContacts( [
		'contacts' => [
			[
				'_' => 'inputPhoneContact' ,
				'client_id' => 0 ,
				'phone' => $phone ,
				'first_name' => '' ,
				'last_name' => ''
			]
		]
	] ) ;

	foreach ( $contacts[ 'imported' ] as $contact ) {
		$mph->messages->sendMessage( [
			'peer' => $contact[ 'user_id' ] ,
			'message' => $msg ,
			'parse_mode' => 'Markdown' ,
		] ) ;
	}
